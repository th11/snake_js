# Snake

The popular Snake game. The goal is to eat as many apples without leaving the board or hitting the body of the snake. Each time you eat an apple, the snake gets longer. As you level up, the snake will move faster. Enjoy!

Use arrow keys to change the direction of the snake. Press 's' to start the game, 'p' to pause, or 'r' to restart.

### [Live Demo][weburl]

[weburl]: https://th11.github.io/snake_js/html/snake.html

## Features
- Pause / resume
- Personal high-score (tracked via cookie)
- Increasing difficult (levels)

## Todos
- Option to specify size of the board.
- Add a back-end to track high-scores. Don't use cookies.

## Screenshot
![ss]

[ss]: ./images/snake.png
