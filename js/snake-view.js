(function () {
  if (typeof SG === "undefined") {
    window.SG = {};
  }

  var View = SG.View = function (options) {
    this.$el = $(options.el);
    this.$instructions = $(options.instructions);
    this.$score = $(options.score);
    this.$level = $(options.level);
    this.$highScore = $(options.highScore);
    this.$prevScore = $(options.prevScore);
    this.level = 0;
    this.highScore = 0;
    this.pause = true;
    this.score = 0;

    this.board = new SG.Board(20);
    this.setupGrid();
    this.render();
    this.setHighScore();
    this.resetInstructions();

    $(document).on("keydown", this.handleKeyEvent.bind(this));
  };

  View.KEYS = {
    38: "N",
    39: "E",
    40: "S",
    37: "W"
  };

  View.prototype.run = function () {
    var view = this;
    setTimeout( function () {
      view.step();
      if( !view.pause ) {
        view.run();
      }
    }, this.board.snake.speed);
  };

  View.prototype.handleKeyEvent = function (event) {
    if (View.KEYS[event.keyCode]) {
      this.board.snake.turn(View.KEYS[event.keyCode]);
      return;
    }

    switch(event.keyCode) {
      case 80: // 'p'
        this.pause = !this.pause;
        if ( !this.pause ) {
          this.run();
        }
        break;
      case 83: //'s'
        if ( this.pause ) {
          this.pause = !this.pause;
          this.run();
        }
        break;
      case 70: // 'f'
        this.board.snake.increaseSpeed();
        break;
      case 71: // 'f'
        this.board.snake.resetSpeed();
        break;
      case 82: // 'r'
        this.pause = true;
        SG.createGame();
        break;
      default:
        return;
    }
  };

  View.prototype.resetInstructions = function() {
    this.$instructions.text("Press 's' to start. Press 'p' to pause. Use arrow keys to change direction.");
    this.$score.text(0);
    this.$level.text(1);
  };

  View.prototype.render = function () {
    this.updateClasses(this.board.snake.segments, "snake");
    this.updateClasses([this.board.apple.position], "apple");
  };

  View.prototype.updateClasses = function(coords, className) {
    this.$li.filter("." + className).removeClass();

    coords.forEach(function(coord){
      var flatCoord = (coord.row * this.board.dim) + coord.col;
      this.$li.eq(flatCoord).addClass(className);
    }.bind(this));
  };

  View.prototype.setupGrid = function () {
    var html = "";
    for (var i = 0, n = this.board.dim; i < n; i++) {
      html += "<ul>";
      for (var j = 0; j < n; j++) {
        html += "<li></li>";
      }
      html += "</ul>";
    }

    this.$el.html(html);
    this.$li = this.$el.find("li");
  };

  View.prototype.gameOver = function() {
    if (!this.board.snake.alive) {
      return true;
    }
    return false;
  };

  View.prototype.step = function () {
    if (!this.gameOver()) {
      this.board.snake.move();
      this.render();
      this.updateScore();
    } else {
      this.endGameProcedure();
      this.pause = true;
    }
  };

  View.prototype.endGameProcedure = function () {
    this.$li.addClass("game-over");

    if (this.score > this.highScore) {
        this.highScore = this.score;
        docCookies.setItem('snake-high-score', this.highScore.toString());
    }
    this.$prevScore.text(this.score);
    this.$instructions.text("Game Over! Press 'r' to restart.");
  };

  View.prototype.setHighScore = function() {
    var cookie = docCookies.getItem('snake-high-score');
    if (cookie) {
      this.highScore = parseInt(cookie);
    }

    this.$highScore.text(this.highScore);
  };

  View.prototype.updateLevel = function () {
    if (this.score > 0 && this.score % 5 === 0) {
      this.level++;
      this.$level.text(this.level);
      this.board.snake.increaseSpeed();
    }
  };

  View.prototype.updateScore = function () {
    var snakeLength = this.board.snake.segments.length - 1;
    if (!this.gameOver() && snakeLength > this.score) {
      this.score = snakeLength;
      this.$score.text(this.score);
      this.updateLevel();
    }
  };

  SG.createGame = function() {
    $(document).off('keydown');
    $(".wrapper").empty();
    new SG.View({
      el: ".wrapper",
      instructions: ".instructions",
      score: "#score",
      level: "#level",
      prevScore: "#previous-score",
      highScore: "#high-score"
    });
  };
})();
